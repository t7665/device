# Device

This repository contains the device code for the open ears device, as a part of my Master's Thesis.

## Files

It contains the following (relevant) files:

- capture.py - Contains the main code
- capture_mqtt.py - Contains the code for sending results over mqtt
- config.py - Contains configurations
