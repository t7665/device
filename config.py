# File in which the vggish weights can be found
vggish_weights = "../vggish/vggish_audioset_weights.h5"

# Directory in which pallas model can be found
model_directory = "../pallas/model_10"

# Name of the recording device
device_name = "Umik-1"

# Whether to override the amount of channels, set to 0 to use device channels
channels = 1

# Whether to print timing information
print_times = True
