import pyaudio
import multiprocessing as mp
import itertools
import config
import datetime
from queue import Full
import sys

import tensorflow as tf

from vggish.vggish import VGGish
from vggish.preprocess_sound import preprocess_sound

def get_device_by_name(audio, name):
    """
    Return the device with the given name.
    
    Parameters
    ----------
    audio : pyaudio.PyAudio
        The PyAudio object handling audio I/O
    name : str
        The name of the device
    """
    for i in range(audio.get_device_count()):
        device = audio.get_device_info_by_index(i)
        if device['name'].startswith(name):
            return device
    raise ValueError("Device not found")

class Streamspec():
    """
    A specification for an audio stream, context manager
    """
    def __init__(self, audio, **kwargs):
        """
        Initialize the stream specification
        
        Parameters
        ----------
        audio : pyaudio.PyAudio
            The PyAudio object handling audio I/O
        Any parameters accepted by audio.open
        """
        self.audio = audio
        self.kwargs = kwargs
        self.stream = None
    
    def __enter__(self):
        """
        Create an audio stream with the given specifications
        
        Returns
        -------
        pyaudio.Stream
            The audio stream
        """
        if self.stream is not None:
            self.stream.stop_stream()
            self.stream.close()
        self.stream = self.audio.open(**self.kwargs)
        return self.stream
    
    def __exit__(self, exc_type, exc_value, exc_traceback):
        """
        Close the audio stream
        
        Parameters
        ----------
        exc_type : Class
            The exception type, None if no exception occurred
        exc_value : str
            The exception value, None if no exception occurred
        exc_traceback : traceback
            The exception's traceback, None if no exception occurred
        """
        # Print abnormal exits
        if not (exc_type is None and exc_val is None and exc_tb is None):
            print(exc_type, exc_val, exc_tb)
        self.stream.stop_stream()
        self.stream.close()
        self.stream = None

def listen_thread(queue, duration, streamspec):
    """
    Blocking listen function, use with threading
    
    Parameters
    ----------
    queue : multiprocessing.Queue
        The queue for communicating between processes/threads
    duration : int
        Amount of buffers to listen to, if 0, read continuously
    streamspec : Streamspec
        The I/O audio stream specification
    """
    if config.print_times:
        print(f'{datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]} start listening')
    try:
        with streamspec as stream:
            for i in itertools.count():
                if i > duration and duration != 0:
                    return
                frames = stream.read(streamspec.kwargs['frames_per_buffer'])
                queue.put_nowait(frames)
                if config.print_times:
                    print(f'{datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]} Recorded {len(frames)} bytes, qsize={queue.qsize()}')
    except Full as e:
        print('Queue overflowed, inference cannot keep up with recording, please upgrade device')
        # Emtpy the queue and exit
        while not queue.empty():
            queue.get()
        sys.exit()

def infer_loop(queue):
    """
    Blocking inference function, use with threading
    
    Parameters
    ----------
    queue : multiprocessing.Queue
        The queue for communicating between processes/threads
    """
    if config.print_times:
        print(f'{datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]} start inferring')
    received = []
    while True:
        frames = queue.get()
        if config.print_times:
            print(f'{datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]} Received {len(frames)} bytes')
        received += [frames]
        if len(received) >= 10:
            final_frames = b''.join(received)
            infer(final_frames)
            received = []

def load_vggish(vggish_weights):
    """
    Load the vggish model into memory
    
    Parameters
    ----------
    vggish_directory : str
        The folder in which the vggish model can be found
    """
    global vggish
    # vggish = VGGish(vggish_weights)

def load_model(model_directory):
    """
    Load the pallas/serval model into memory
    
    Parameters
    ----------
    model_directory : str
        The folder in which the pallas/serval model can be found
    """
    global model
    model = tf.keras.models.load_model(model_directory, custom_objects={'LSTM_Network': tf.keras.models.Sequential})

def preprocess(frames):
    """
    Preprocess the given frames
    
    Parameters
    ----------
    frames : bytes
        The frames as read from the microphone
    """
    return preprocess_sound(frames)

def infer(frames):
    """
    Run interference on the given frames
    
    Parameters
    ----------
    frames : bytes
        The frames as read from the microphone
    """
    print(f'Running inference on {len(frames)} bytes')
    preprocessed = preprocess(frames)
    # do inference on preprocessed
    import time
    time.sleep(2)
    print("TODO: Implement inference")

def run_capture(
    mqtt_publisher=None,
    device_name=config.device_name,
    vggish_directory=config.vggish_weights,
    model_directory=config.model_directory,
):
    """
    Run the capture, optionally with MQTT
    
    Parameters
    ----------
    mqtt_publisher : function, optional
        Function that handles MQTT and sends a single message (default is None)
    device_name : str, optional
        Name of the microphone (default is config.device_name)
    vggish_directory : str, optional
        Directory of the vggish model
    model_directory : str, optional
        Directory of the pallas/serval model
    """
    audio = pyaudio.PyAudio()
    vggish = load_vggish(vggish_directory)
    model = load_model(model_directory)
    
    audio_device = get_device_by_name(audio, device_name)
    samplerate = int(audio_device['defaultSampleRate'])
    channels = config.channels if config.channels > 0 else audio_device['maxInputChannels']
    framebuffersize = samplerate # 1 second per buffer
    
    streamspec = Streamspec(
        audio,
        input_device_index = audio_device['index'],
        input = True,
        format = pyaudio.paInt16,
        rate = samplerate,
        channels = channels,
        frames_per_buffer = framebuffersize,
    )
    
    queue = mp.Queue(5)
    try:
        listenProc = mp.Process(target=listen_thread, args=(queue, 0, streamspec))
        listenProc.start()
        infer_loop(queue)
    except KeyboardInterrupt:
        pass
    finally:
        listenProc.kill()
        listenProc.join()

if __name__ == '__main__':
    run_capture()
